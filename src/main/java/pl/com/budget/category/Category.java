package pl.com.budget.category;

import lombok.*;
import pl.com.budget.receipt.Categories;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class Category {
    private Categories category;
    private double sumOfMoney;

}
