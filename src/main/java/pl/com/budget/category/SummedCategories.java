package pl.com.budget.category;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class SummedCategories {
  private List<Category> categories;

}
